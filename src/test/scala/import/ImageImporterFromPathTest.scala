package `import`

import java.io.File

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

import image.{ImmutableImage, ImmutablePixelGrid, RGBPixel}
import org.scalatest.FunSuite

class ImageImporterFromPathTest extends FunSuite {

  test("testImportImage") {
    val out = new BufferedImage(2, 2, BufferedImage.TYPE_INT_RGB)
    out.setRGB(0, 0, 65536 * 255 + 256 * 255 + 1 * 255)
    out.setRGB(0, 1, 65536 * 255 + 256 * 255 + 1 * 255)
    out.setRGB(1, 0, 65536 * 255 + 256 * 255 + 1 * 255)
    out.setRGB(1, 1, 65536 * 255 + 256 * 255 + 1 * 255)
    ImageIO.write(out, "jpg", new File("./src/test/resources/test.jpg"))

    val outputGrid = Array.ofDim[RGBPixel](2, 2)
    outputGrid(0)(0) = RGBPixel(255, 255, 255)
    outputGrid(0)(1) = RGBPixel(255, 255, 255)
    outputGrid(1)(0) = RGBPixel(255, 255, 255)
    outputGrid(1)(1) = RGBPixel(255, 255, 255)
    val outputImage = new ImmutableImage[RGBPixel](new ImmutablePixelGrid[RGBPixel](outputGrid))

    val loadedImage = new ImageImporterFromPath("./src/test/resources/test.jpg").importImage

    for (i <- 0 until loadedImage.height) {
      for (j <- 0 until loadedImage.width) {
        assert(loadedImage.getPixel(i, j).red == outputImage.getPixel(i, j).red)
        assert(loadedImage.getPixel(i, j).green == outputImage.getPixel(i, j).green)
        assert(loadedImage.getPixel(i, j).blue == outputImage.getPixel(i, j).blue)
      }
    }

  }
  test("import wrong type") {
    assertThrows[IllegalArgumentException] {
      new ImageImporterFromPath("test.aaa")
    }
  }

}

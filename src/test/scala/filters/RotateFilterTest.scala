package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}
import org.scalatest.FunSuite

class RotateFilterTest extends FunSuite {

  test("testRotate degree%90!=0") {
    assertThrows[IllegalArgumentException] {
      RotateFilter(12)
    }
  }

  test("testRotate +90") {
    val inputGrid = Array.ofDim[GreyPixel](3, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)
    inputGrid(2)(0) = GreyPixel(20)
    inputGrid(2)(1) = GreyPixel(21)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 3)
    outputGrid(0)(0) = GreyPixel(20)
    outputGrid(0)(1) = GreyPixel(10)
    outputGrid(0)(2) = GreyPixel(0)
    outputGrid(1)(0) = GreyPixel(21)
    outputGrid(1)(1) = GreyPixel(11)
    outputGrid(1)(2) = GreyPixel(1)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(90).applyFilter(inputImage)

    for (i <- 0 until outputImage.height) {
      for (j <- 0 until outputImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate -270") {
    val inputGrid = Array.ofDim[GreyPixel](3, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)
    inputGrid(2)(0) = GreyPixel(20)
    inputGrid(2)(1) = GreyPixel(21)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 3)
    outputGrid(0)(0) = GreyPixel(20)
    outputGrid(0)(1) = GreyPixel(10)
    outputGrid(0)(2) = GreyPixel(0)
    outputGrid(1)(0) = GreyPixel(21)
    outputGrid(1)(1) = GreyPixel(11)
    outputGrid(1)(2) = GreyPixel(1)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(-270).applyFilter(inputImage)

    for (i <- 0 until outputImage.height) {
      for (j <- 0 until outputImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate +180") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 1)
    outputGrid(0)(0) = GreyPixel(10)
    outputGrid(1)(0) = GreyPixel(0)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(180).applyFilter(inputImage)

    for (i <- 0 until outputImage.height) {
      for (j <- 0 until outputImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate -180") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 1)
    outputGrid(0)(0) = GreyPixel(10)
    outputGrid(1)(0) = GreyPixel(0)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(-180).applyFilter(inputImage)

    for (i <- 0 until outputImage.height) {
      for (j <- 0 until outputImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate 360") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))
    val rotatedImage = RotateFilter(360).applyFilter(inputImage)
    for (i <- 0 until rotatedImage.height) {
      for (j <- 0 until rotatedImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == inputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate -360") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))
    val rotatedImage = RotateFilter(-360).applyFilter(inputImage)
    for (i <- 0 until rotatedImage.height) {
      for (j <- 0 until rotatedImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == inputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate -90") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](1, 2)
    outputGrid(0)(0) = GreyPixel(0)
    outputGrid(0)(1) = GreyPixel(10)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(-90).applyFilter(inputImage)
    for (i <- 0 until rotatedImage.height) {
      for (j <- 0 until rotatedImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate 270") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](1, 2)
    outputGrid(0)(0) = GreyPixel(0)
    outputGrid(0)(1) = GreyPixel(10)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(270).applyFilter(inputImage)
    for (i <- 0 until rotatedImage.height) {
      for (j <- 0 until rotatedImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testRotate >360") {
    val inputGrid = Array.ofDim[GreyPixel](2, 1)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(10)
    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 1)
    outputGrid(0)(0) = GreyPixel(10)
    outputGrid(1)(0) = GreyPixel(0)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val rotatedImage = RotateFilter(540).applyFilter(inputImage)

    for (i <- 0 until outputImage.height) {
      for (j <- 0 until outputImage.width) {
        assert(rotatedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
}

package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}
import org.scalatest.FunSuite

class InvertFilterTest extends FunSuite {

  test("testInvert ") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(255)
    outputGrid(0)(1) = GreyPixel(254)
    outputGrid(1)(0) = GreyPixel(245)
    outputGrid(1)(1) = GreyPixel(244)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = InvertFilter().applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testInvert all 255") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(255)
    inputGrid(0)(1) = GreyPixel(255)
    inputGrid(1)(0) = GreyPixel(255)
    inputGrid(1)(1) = GreyPixel(255)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(0)
    outputGrid(0)(1) = GreyPixel(0)
    outputGrid(1)(0) = GreyPixel(0)
    outputGrid(1)(1) = GreyPixel(0)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = InvertFilter().applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testInvert all 0") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(0)
    inputGrid(1)(0) = GreyPixel(0)
    inputGrid(1)(1) = GreyPixel(0)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(255)
    outputGrid(0)(1) = GreyPixel(255)
    outputGrid(1)(0) = GreyPixel(255)
    outputGrid(1)(1) = GreyPixel(255)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = InvertFilter().applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }
}

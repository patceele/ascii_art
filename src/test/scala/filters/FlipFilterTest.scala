package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}
import org.scalatest.FunSuite

class FlipFilterTest extends FunSuite {

  test("testFlip x") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(10)
    outputGrid(0)(1) = GreyPixel(11)
    outputGrid(1)(0) = GreyPixel(0)
    outputGrid(1)(1) = GreyPixel(1)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = FlipFilter(x = true, y = false).applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }

  }
  test("testFlip twice x ") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))
    val flippedImage = FlipFilter(x = true, y = false).applyFilter(FlipFilter(x = true, y = false).applyFilter(inputImage))

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == inputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testFlip not x not y") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))
    val flippedImage = FlipFilter(x = false, y = false).applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == inputImage.getPixel(i, j).grey)
      }
    }
  }
  test("testFlip y") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(1)
    outputGrid(0)(1) = GreyPixel(0)
    outputGrid(1)(0) = GreyPixel(11)
    outputGrid(1)(1) = GreyPixel(10)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = FlipFilter(x = false, y = true).applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }

  }
  test("testFlip x and y") {
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(0)
    inputGrid(0)(1) = GreyPixel(1)
    inputGrid(1)(0) = GreyPixel(10)
    inputGrid(1)(1) = GreyPixel(11)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(11)
    outputGrid(0)(1) = GreyPixel(10)
    outputGrid(1)(0) = GreyPixel(1)
    outputGrid(1)(1) = GreyPixel(0)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val flippedImage = FlipFilter(x = true, y = true).applyFilter(inputImage)

    for (i <- 0 until flippedImage.height) {
      for (j <- 0 until flippedImage.width) {
        assert(flippedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }

  }

}

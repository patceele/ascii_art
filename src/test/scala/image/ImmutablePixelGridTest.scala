package image

import org.scalatest.FunSuite

class ImmutablePixelGridTest extends FunSuite {

  test("testPixelGrid width=0 ") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](1, 0))
    }
  }
  test("testPixelGrid height=0 ") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](0, 1))
    }
  }

  test("testPixelGrid width[0]!=width[1]") {
    assertThrows[IllegalArgumentException] {
      val array = Array(Array(null, null, null), Array(null, null))
      ImmutablePixelGrid(array)
    }
  }
  test("testGetPixel row<0") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](2, 2)).getPixel(-1, 0)
    }
  }
  test("testGetPixel column<0") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](2, 2)).getPixel(0, -5)
    }
  }
  test("testGetPixel row>height") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](2, 2)).getPixel(5, 0)
    }
  }
  test("testGetPixel column>width") {
    assertThrows[IllegalArgumentException] {
      ImmutablePixelGrid(Array.ofDim[RGBPixel](2, 2)).getPixel(0, 4)
    }
  }

}

package `export`

import java.io.{File, FileOutputStream}

import image.{AsciiPixel, ImmutableImage, ImmutablePixelGrid}
import org.scalatest.FunSuite

class ImageExporterTest extends FunSuite {

  test("testSave") {

    val outputGrid = Array.ofDim[AsciiPixel](2, 2)
    outputGrid(0)(0) = AsciiPixel('@')
    outputGrid(0)(1) = AsciiPixel('+')
    outputGrid(1)(0) = AsciiPixel('%')
    outputGrid(1)(1) = AsciiPixel(':')
    val outputImage = new ImmutableImage[AsciiPixel](new ImmutablePixelGrid[AsciiPixel](outputGrid))

    val exporter = new ImageExporter
    exporter.save(outputImage, new FileOutputStream("./src/test/resources/exportTest.txt"))
    assert(new File("exportTest.txt").compareTo(new File("exportTest.txt")) == 0)
  }

}

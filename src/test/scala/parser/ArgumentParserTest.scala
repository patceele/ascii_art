package parser

import filters.{Filter, FlipFilter, InvertFilter, RotateFilter}
import org.scalatest.FunSuite

class ArgumentParserTest extends FunSuite {

  //GetFilters test
  test("testGetFilters") {
    val args: Array[String] = Array[String]("--image", "sm.gif", "--flip", "x", "--invert", "--flip", "y", "--rotate", "-90", "--output-console", "--output-file", "outTest2.txt")
    val parser = new ArgumentParser(args)
    val filters: Array[Filter] = Array[Filter](new FlipFilter(true, false), new InvertFilter, new FlipFilter(false, true), new RotateFilter(-90))
    assert(filters.sameElements(parser.getFilters))
  }
  test("testGetFilters no filters") {
    val args: Array[String] = Array[String]("--image", "sm.gif", "--output-console")
    val parser = new ArgumentParser(args)
    val filters: Array[Filter] = Array[Filter]()
    assert(filters.sameElements(parser.getFilters))
  }
  test("testGetFilters no arg for flip filter") {
    val args: Array[String] = Array[String]("--image", "sm.gif", "--flip", "--output-console")
    val parser = new ArgumentParser(args)
    val filters: Array[Filter] = Array[Filter]()
    assertThrows[Exception] {
      parser.getFilters
    }
  }
  test("testGetFilters no arg for rotate filter") {
    val args: Array[String] = Array[String]("--image", "sm.gif", "--rotate")
    val parser = new ArgumentParser(args)
    val filters: Array[Filter] = Array[Filter]()
    assertThrows[Exception] {
      parser.getFilters
    }
  }


  //IsConsoleExport test
  test("testIsConsoleExport no args") {
    val args: Array[String] = Array[String]()
    val parser = new ArgumentParser(args)
    assert(!parser.isConsoleExport)
  }
  test("testIsConsoleExport right args") {
    val args: Array[String] = Array[String]("--image", "\"sm.gif\"", "--flip", "x", "--invert", "--output-console", "--output-file", "outTest2.txt")
    val parser = new ArgumentParser(args)
    assert(parser.isConsoleExport)
  }
  test("testIsConsoleExport wrong args") {
    val args: Array[String] = Array[String]("--image", "sm.gif", "--outputWR-console", "--output-file", "outTest2.txt")
    val parser = new ArgumentParser(args)
    assert(!parser.isConsoleExport)
  }


  //GetImagePath test
  test("testGetImagePath wrong first arg") {
    val args: Array[String] = Array[String]("first_arg", "--image", "sm.gif", "--flip", "x", "--invert", "--output-console", "--output-file", "outTest2.txt")
    val parser = new ArgumentParser(args)
    assertThrows[Exception] {
      parser.getImagePath
    }
  }
  test("testGetImagePath no args") {
    val args: Array[String] = Array[String]()
    val parser = new ArgumentParser(args)
    assertThrows[Exception] {
      parser.getImagePath
    }
  }
  test("testGetImagePath only one arg") {
    val args: Array[String] = Array[String]("--image")
    val parser = new ArgumentParser(args)
    assertThrows[Exception] {
      parser.getImagePath
    }
  }
  test("testGetImagePath") {
    val args: Array[String] = Array[String]("--image", "test_img.jpg", "--flip", "x", "--invert", "--output-console", "--output-file", "outTest2.txt")
    val parser = new ArgumentParser(args)
    assert("test_img.jpg" == parser.getImagePath)
  }


  //GetExportPath test
  test("testGetExportPath no path export") {
    val args: Array[String] = Array[String]("--image", "test_img.jpg", "--flip", "x", "--invert")
    val parser = new ArgumentParser(args)
    assert(null == parser.getExportPath)
  }
  test("testGetExportPath ") {
    val args: Array[String] = Array[String]("--image", "test_img.jpg", "--flip", "x", "--invert", "--output-file", "outputTest.txt")
    val parser = new ArgumentParser(args)
    assert("outputTest.txt" == parser.getExportPath)
  }
  test("testGetExportPath no path") {
    val args: Array[String] = Array[String]("--image", "test_img.jpg", "--output-file")
    val parser = new ArgumentParser(args)
    assertThrows[Exception] {
      parser.getExportPath
    }
  }


}

package convert

import image.{AsciiPixel, GreyPixel, ImmutableImage, ImmutablePixelGrid, RGBPixel}
import org.scalatest.FunSuite

class ConverterTest extends FunSuite {

  test("testСonvertToGreyImage") {

    val inputGrid = Array.ofDim[RGBPixel](2, 2)
    inputGrid(0)(0) = RGBPixel(1, 2, 3)
    inputGrid(0)(1) = RGBPixel(4, 5, 6)
    inputGrid(1)(0) = RGBPixel(7, 8, 9)
    inputGrid(1)(1) = RGBPixel(10, 11, 12)

    val inputImage = new ImmutableImage[RGBPixel](new ImmutablePixelGrid[RGBPixel](inputGrid))

    val outputGrid = Array.ofDim[GreyPixel](2, 2)
    outputGrid(0)(0) = GreyPixel(1)
    outputGrid(0)(1) = GreyPixel(4)
    outputGrid(1)(0) = GreyPixel(7)
    outputGrid(1)(1) = GreyPixel(10)
    val outputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](outputGrid))

    val convertedImage = new Converter().convertToGreyImage(inputImage)

    for (i <- 0 until convertedImage.height) {
      for (j <- 0 until convertedImage.width) {
        assert(convertedImage.getPixel(i, j).grey == outputImage.getPixel(i, j).grey)
      }
    }
  }

  test("testСonvertToAscii") {
    //"@%#*+=-:. "
    val inputGrid = Array.ofDim[GreyPixel](2, 2)
    inputGrid(0)(0) = GreyPixel(1)
    inputGrid(0)(1) = GreyPixel(120)
    inputGrid(1)(0) = GreyPixel(40)
    inputGrid(1)(1) = GreyPixel(200)

    val inputImage = new ImmutableImage[GreyPixel](new ImmutablePixelGrid[GreyPixel](inputGrid))

    val outputGrid = Array.ofDim[AsciiPixel](2, 2)
    outputGrid(0)(0) = AsciiPixel('@')
    outputGrid(0)(1) = AsciiPixel('+')
    outputGrid(1)(0) = AsciiPixel('%')
    outputGrid(1)(1) = AsciiPixel(':')
    val outputImage = new ImmutableImage[AsciiPixel](new ImmutablePixelGrid[AsciiPixel](outputGrid))

    val convertedImage = new Converter().convertToAscii(inputImage, new SimpleConvertTable())

    for (i <- 0 until convertedImage.height) {
      for (j <- 0 until convertedImage.width) {
        assert(convertedImage.getPixel(i, j).char == outputImage.getPixel(i, j).char)
      }
    }
  }
}

package convert

import image.{AsciiPixel, GreyPixel}
import org.scalatest.FunSuite

class ConvertTableTest extends FunSuite {

  test("testGreyToAscii -value") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    assert(new AsciiPixel('@') == table.greyToAscii(new GreyPixel(-3)))
  }
  test("testGreyToAscii 0 value") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    assert(new AsciiPixel('@') == table.greyToAscii(new GreyPixel(0)))
  }
  test("testGreyToAscii >255 value") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    assert(new AsciiPixel(' ') == table.greyToAscii(new GreyPixel(270)))
  }
  test("testGreyToAscii 255 value") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    assert(new AsciiPixel(' ') == table.greyToAscii(new GreyPixel(255)))
  }
  test("testGreyToAscii @") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    for (i <- 0 until 26) {
      assert(new AsciiPixel('@') == table.greyToAscii(new GreyPixel(i)))
    }
  }
  test("testGreyToAscii +") {
    val table = new SimpleConvertTable() //"@%#*+=-:. "
    for (i <- 103 until 127) {
      assert(new AsciiPixel('+') == table.greyToAscii(new GreyPixel(i)))
    }
  }


}

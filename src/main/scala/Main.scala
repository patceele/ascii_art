import java.io.FileOutputStream

import `export`.ImageExporter
import `import`.ImageImporterFromPath

import convert.{Converter, SimpleConvertTable, StandardConvertTable}
import parser.ArgumentParser

//  Example of usage:
//     run --image <image_path> --output-console
//     run --image <image_path> --output-file output.txt
//     run --image <image_path> --rotate +90 --invert --rotate -270 --output-file output.txt --output-console
//     run --image <image_path> --rotate +90 --invert --flip x --output-file output.txt --output-console
object Main {


  def main(args: Array[String]): Unit = {

    val parser = new ArgumentParser(args)
    val exporter = new ImageExporter
    val converter = new Converter

    val rgbImage = new ImageImporterFromPath(parser.getImagePath).importImage
    var greyImage = converter.convertToGreyImage(rgbImage)
    for (filter <- parser.getFilters) {
      greyImage = filter.applyFilter(greyImage)
    }
    //        val asciiImage = converter.convertToAscii(greyImage, new StandardConvertTable())
    val asciiImage = converter.convertToAscii(greyImage, new SimpleConvertTable())
    if (parser.isConsoleExport) {
      exporter.save(asciiImage, System.out)
    }
    val exportPath = parser.getExportPath
    if (exportPath != null) {
      exporter.save(asciiImage, new FileOutputStream(exportPath))
    }
  }

}

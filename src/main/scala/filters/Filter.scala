package filters

import image.{GreyPixel, ImmutableImage}

/**
 * common trait fot all filters
 */
trait Filter {

  def applyFilter(image: ImmutableImage[GreyPixel]): ImmutableImage[GreyPixel]
}

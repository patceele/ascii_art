package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}

import scala.reflect.ClassTag

/** Flip Filter
 *
 * @param x true if image will flipped by x axes
 * @param y true if image will flipped by y axes
 */
case class FlipFilter(x: Boolean, y: Boolean) extends Filter {

  /**
   * flip image
   *
   * @param image image to flip
   * @return flipped image
   */
  override def applyFilter(image: ImmutableImage[GreyPixel]): ImmutableImage[GreyPixel] = {
    flip(image)
  }

  private def flip[T: ClassTag](image: ImmutableImage[T]): ImmutableImage[T] = {
    var newImage: ImmutableImage[T] = image
    if (x) {
      newImage = flipX(newImage)
    }
    if (y) {
      newImage = flipY(newImage)
    }
    newImage
  }

  private def flipY[T: ClassTag](image: ImmutableImage[T]): ImmutableImage[T] = {
    val newImageGrid = Array.ofDim[T](image.height, image.width)
    for (i <- 0 until image.height) {
      for (j <- 0 until image.width) {
        newImageGrid(i)(j) = image.getPixel(i, image.width - j - 1)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[T](newImageGrid))
  }

  private def flipX[T: ClassTag](image: ImmutableImage[T]): ImmutableImage[T] = {
    val newImageGrid = Array.ofDim[T](image.height, image.width)
    for (i <- 0 until image.height) {
      for (j <- 0 until image.width) {
        newImageGrid(i)(j) = image.getPixel(image.height - i - 1, j)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[T](newImageGrid))
  }


}

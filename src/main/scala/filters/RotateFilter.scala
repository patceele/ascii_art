package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}

import scala.reflect.ClassTag

/**
 * Rotation filter
 *
 * @param degree rotation degree
 */
case class RotateFilter(degree: Integer) extends Filter {

  /** Rotate image by degree
   *
   * @param image image to rotate
   * @return rotated image
   */
  override def applyFilter(image: ImmutableImage[GreyPixel]): ImmutableImage[GreyPixel] = {
    rotate(image)
  }

  if (degree % 90 != 0) {
    throw new IllegalArgumentException()
  }

  private def rotate90[T: ClassTag](image: ImmutableImage[T]): ImmutableImage[T] = {
    val newImage = Array.ofDim[T](image.width, image.height)
    for (i <- 0 until image.width) {
      for (j <- 0 until image.height) {
        newImage(i)(j) = image.getPixel(image.height - j - 1, i)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[T](newImage))
  }

  private def rotate[T: ClassTag](image: ImmutableImage[T]): ImmutableImage[T] = {
    var rotation = degree
    if (rotation >= 360) {
      rotation = rotation - (rotation / 360) * 360
    }
    if (rotation < 0) {
      rotation = 360 + rotation
    }
    var newImage: ImmutableImage[T] = image
    while (rotation > 0 && rotation < 360) {
      newImage = rotate90(newImage)
      rotation -= 90
    }
    newImage
  }
}

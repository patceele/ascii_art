package filters

import image.{GreyPixel, ImmutableImage, ImmutablePixelGrid}

/**
 * Invert filter
 */
case class InvertFilter() extends Filter {

  /**
   * invert image
   *
   * @param image image to invert
   * @return inverted image
   */
  override def applyFilter(image: ImmutableImage[GreyPixel]): ImmutableImage[GreyPixel] = {
    invert(image)
  }

  private def invert(image: ImmutableImage[GreyPixel]): ImmutableImage[GreyPixel] = {
    val newImage = Array.ofDim[GreyPixel](image.height, image.width)
    for (i <- 0 until image.height) {
      for (j <- 0 until image.width) {
        newImage(i)(j) = new GreyPixel(255 - image.getPixel(i, j).grey)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[GreyPixel](newImage))
  }


}

package `export`

import java.io.OutputStream

import image.{AsciiPixel, ImmutableImage}

/**
 * Export Ascii image to output stream
 */

class ImageExporter {
  /**
   * Export Ascii image to txt file
   *
   * @param image ascii image
   * @param out   output stream
   */

  def save(image: ImmutableImage[AsciiPixel], out: OutputStream): Unit = {
    for (x <- 0 until image.height) {
      for (y <- 0 until image.width) {
        out.write(image.getPixel(x, y).char)
      }
      out.write("\r\n".getBytes("UTF-8"))
    }
  }
}

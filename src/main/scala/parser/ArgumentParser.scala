package parser

import filters.{Filter, FlipFilter, InvertFilter, RotateFilter}

/**
 * Program arguments parser
 *
 * @param args program arguments
 */
class ArgumentParser(args: Array[String]) {

  /**
   * get image path
   *
   * @return image path
   */
  def getImagePath: String = {
    if (args.length <= 2 || args(0) != "--image") {
      throw new Exception("Image path not found")
    }
    args(1)
  }

  /**
   *
   * @return array of filters
   */
  def getFilters: Array[Filter] = {
    var filters = Array[Filter]()
    var i = 2
    while (i < args.length) {
      args(i) match {
        case "--rotate" =>
          if (i + 1 >= args.length) {
            throw new Exception("Rotate argument not found")
          }
          filters = filters :+ new RotateFilter(args(i + 1).toInt)
          i += 1
        case "--flip" =>
          val flipArg = args(i + 1).toLowerCase()
          if (flipArg != "x" && flipArg != "y") {
            throw new Exception("Flip argument no found(use x or y)")
          }
          if (flipArg.equals("x")) {
            filters = filters :+ new FlipFilter(true, false)
          } else {
            filters = filters :+ new FlipFilter(false, true)
          }
          i += 1
        case "--invert" =>
          filters = filters :+ new InvertFilter()
        case _ =>
      }
      i += 1
    }
    filters
  }

  /**
   *
   * @return true if user want to print result to console
   */
  def isConsoleExport: Boolean = {
    args.contains("--output-console")
  }

  /**
   *
   * @return export path
   */
  def getExportPath: String = {
    if (!args.contains("--output-file")) {
      null
    } else {
      val index = args.indexOf("--output-file")
      if (index + 1 >= args.length) {
        throw new Exception("Export path not found")
      }
      args(index + 1)
    }
  }


}

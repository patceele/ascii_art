package convert

import image.{AsciiPixel, GreyPixel}

/** Table to convert GreyPixel to AsciiPixel
 *
 * @param convertString
 */
abstract class ConvertTable(convertString: String) {
  var size: Int = convertString.length

  /** Convert GreyPixel to AsciiPixel
   *
   * @param in input GreyPixel
   * @return AsciiPixel
   */
  def greyToAscii(in: GreyPixel): AsciiPixel = {
    var greyValue = in.grey
    if (greyValue < 0) {
      greyValue = 0
    } else if (greyValue > 255) {
      greyValue = 255
    }
    val separation = (256.toDouble / size)
    new AsciiPixel(convertString((greyValue / separation).toInt))
  }
}

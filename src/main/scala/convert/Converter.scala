package convert

import image.{AsciiPixel, GreyPixel, ImmutableImage, ImmutablePixelGrid, RGBPixel}

/** Convert rgb image to grey scale image and grey scale image to ascii image
 */
class Converter {
  /** Convert rgb image to grey scale image
   *
   * @param image rgb image to convert
   * @return grey scale image
   */
  def convertToGreyImage(image: ImmutableImage[RGBPixel]): ImmutableImage[GreyPixel] = {
    val greyPixels = Array.ofDim[GreyPixel](image.height, image.width)
    for (i <- 0 until image.height) {
      for (j <- 0 until image.width) {
        val red = image.getPixel(i, j).red
        val green = image.getPixel(i, j).green
        val blue = image.getPixel(i, j).blue
        greyPixels(i)(j) = new GreyPixel((red * 0.3 + green * 0.59 + blue * 0.11).toInt)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[GreyPixel](greyPixels))
  }

  /** Convert grey scale image to ascii image
   *
   * @param image        grey scale image to convert
   * @param convertTable convertation table
   * @return converted ascii image
   */
  def convertToAscii(image: ImmutableImage[GreyPixel], convertTable: ConvertTable): ImmutableImage[AsciiPixel] = {
    val asciiPixels = Array.ofDim[AsciiPixel](image.height, image.width)
    for (i <- 0 until image.height) {
      for (j <- 0 until image.width) {
        asciiPixels(i)(j) = convertTable.greyToAscii(image.getPixel(i, j))
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[AsciiPixel](asciiPixels))
  }

}

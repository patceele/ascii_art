package image

/**
 * common trait for every pixel
 */
trait ImmutablePixel {
}

/**
 * RGB pixel
 *
 * @param red   red value
 * @param green green value
 * @param blue  blue value
 */
case class RGBPixel(red: Int, green: Int, blue: Int) extends ImmutablePixel {}

/**
 * Grey pixel
 *
 * @param grey grey value
 */
case class GreyPixel(grey: Int) extends ImmutablePixel {}

/**
 * Ascii pixel
 *
 * @param char char value
 */
case class AsciiPixel(char: Char) extends ImmutablePixel {}




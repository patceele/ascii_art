package image

/**
 * Represents any project image
 *
 * @param pixelGrid storage of pixels
 * @tparam T type of pixels
 */
case class ImmutableImage[T](private val pixelGrid: ImmutablePixelGrid[T]) {
  def width: Int = pixelGrid.width

  def height: Int = pixelGrid.height

  def getPixel(row: Int, column: Int): T = pixelGrid.getPixel(row, column)
}


package image

/**
 * Storage of pixels
 *
 * @param pixels array of pixels
 * @tparam T type of pixel
 */
case class ImmutablePixelGrid[T](private val pixels: Array[Array[T]]) {
  if (pixels.length == 0)
    throw new IllegalArgumentException()

  if (pixels.exists(e => e.length == 0))
    throw new IllegalArgumentException()

  val height: Int = pixels.length
  val width: Int = pixels(0).length

  if (pixels.exists(e => e.length != width))
    throw new IllegalArgumentException()

  def getPixel(row: Int, column: Int): T = {
    if (row > height - 1 || row < 0)
      throw new IllegalArgumentException();

    if (column > width - 1 || column < 0)
      throw new IllegalArgumentException();

    pixels(row)(column)
  }
}



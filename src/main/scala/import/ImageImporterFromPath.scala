package `import`

import java.io.File

import javax.imageio.ImageIO

import image.{ImmutableImage, ImmutablePixelGrid, RGBPixel}

/**
 * Import from file
 *
 * @param path file path
 */

class ImageImporterFromPath(path: String) extends ImageImporter {

  private val fileType: String = path.split('.').last
  if (!fileType.equals("png") && !fileType.equals("jpg") && !fileType.equals("gif")) {
    throw new IllegalArgumentException()
  }

  /**
   * import image from path
   *
   * @return converted to RGB image
   */
  override def importImage: ImmutableImage[RGBPixel] = {
    loadFromPath()
  }

  private def loadFromPath(): ImmutableImage[RGBPixel] = {
    val bufferedPixels = ImageIO.read(new File(path))
    val arrayPixels = Array.ofDim[RGBPixel](bufferedPixels.getHeight(), bufferedPixels.getWidth())

    for (i <- 0 until bufferedPixels.getHeight()) {
      for (j <- 0 until bufferedPixels.getWidth()) {
        val color = bufferedPixels.getRGB(j, i)
        val red = (color & 0xff0000) / 65536
        val green = (color & 0xff00) / 256
        val blue = (color & 0xff)
        arrayPixels(i)(j) = RGBPixel(red, green, blue)
      }
    }
    new ImmutableImage(new ImmutablePixelGrid[RGBPixel](arrayPixels))
  }

}


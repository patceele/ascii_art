package `import`

import image.{ImmutableImage, RGBPixel}

/**
 * common trait for all image importers
 */
trait ImageImporter {
  def importImage: ImmutableImage[RGBPixel]
}

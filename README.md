**The idea of this project is to load images, translate them into ASCII ART images, optionally apply filters, and save them.**

*Example of usage:*

`run --image <image_path> --output-console`

`run --image <image_path> --output-file output.txt`

`run --image <image_path> --rotate +90 --invert --rotate -270 --output-file output.txt --output-console`

`run --image <image_path> --rotate +90 --invert --flip x --output-file output.txt --output-console`
